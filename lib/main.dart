import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "To Do List",
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

var warna1 = Color(0xFFa572c0);
var warna2 = Color(0xFF6559d4);
var gambarProfil = NetworkImage(
    "https://id.gravatar.com/userimage/24730002/c3774b8e8c2586f9f02322138e286b1a.jpeg");

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: Text("To Do List"),
        centerTitle: true,
        backgroundColorStart: warna2,
        backgroundColorEnd: warna1,
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {},
          )
        ],
        elevation: 0.0,
      ),
      body: ListView(
        children: <Widget>[
          BagianAtas(),
          SizedBox(height: 32.0),
          BagianTengah(),
          SizedBox(height: 40),
          BagianBawah(),
        ],
      ),
    );
  }
}

class BagianAtas extends StatelessWidget {
  const BagianAtas({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 96.0,
                width: 96.0,
                child: CircleAvatar(
                  backgroundImage: gambarProfil,
                ),
              ),
              SizedBox(height: 16.0),
              Text("Berri Primaputra", style: TextStyle(fontSize: 24.0)),
              SizedBox(height: 4.0),
              Text("Goal & Task", style: TextStyle(color: Colors.grey)),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 42.0),
          child: Container(
            height: 4.0,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [warna1, warna2])),
          ),
        ),
        SizedBox(height: 16.0),
        Padding(
          padding: const EdgeInsets.only(left: 32, right: 48),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text("54", style: TextStyle(fontSize: 24)),
                  Text("Daftar", style: TextStyle(color: Colors.grey)),
                  Text("Task", style: TextStyle(color: Colors.grey)),
                ],
              ),
              Column(
                children: <Widget>[
                  Text("38", style: TextStyle(fontSize: 24)),
                  Text("Task", style: TextStyle(color: Colors.grey)),
                  Text("Selesai", style: TextStyle(color: Colors.grey)),
                ],
              ),
              Column(
                children: <Widget>[
                  Text("27", style: TextStyle(fontSize: 24)),
                  Text("Goal", style: TextStyle(color: Colors.grey)),
                  Text("Tercapai", style: TextStyle(color: Colors.grey)),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class BagianTengah extends StatelessWidget {
  const BagianTengah({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(height: 8.0),
        ListTile(
          title: Text("Aktifitas Hari Ini"),
          subtitle: Text("31 Task & Kategori"),
          trailing: ClipOval(
            child: Container(
              height: 40.0,
              width: 40.0,
              color: warna1.withOpacity(0.2),
              child: IconButton(
                icon: Icon(Icons.add),
                color: warna2,
                onPressed: () {},
              ),
            ),
          ),
        ),
        SizedBox(height: 8.0),
        Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Container(
            height: 180.0,
            child: ListView(
              padding: EdgeInsets.all(0.0),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                ItemCard(Icons.event_available, "Keseharian", "3 Task"),
                ItemCard(Icons.next_week, "Pekerjaan", "4 Task"),
                ItemCard(Icons.business, "Side Project", "3 Task"),
                ItemCard(Icons.favorite, "Hobi", "1 Task"),
                ItemCard(Icons.people, "Family", "2 Task"),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class ItemCard extends StatelessWidget {
  final icon;
  final name;
  final tasks;
  const ItemCard(
    this.icon,
    this.name,
    this.tasks,
  );
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        height: 160.0,
        width: 120.0,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [warna1, warna2],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(icon, color: Colors.white),
              Spacer(),
              Text(
                name,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 4.0),
              Text(
                tasks,
                style: TextStyle(
                  color: Colors.white.withOpacity(.6),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BagianBawah extends StatelessWidget {
  const BagianBawah({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      color: warna1.withOpacity(0.2),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text("Security & Data"),
                SizedBox(width: 16.0),
                Text("Pengaturan"),
              ],
            ),
            Text("Bantuan"),
          ],
        ),
      ),
    );
  }
}
